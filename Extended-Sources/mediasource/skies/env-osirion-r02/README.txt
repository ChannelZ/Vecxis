  ------------------------------------------------------------------

	Project::OSiRiON Skybox collection
	
	Release 2

	30 space themed environment maps in Qauke 3 skybox format.
	
	by Stijn "Ingar" Buys <ingar@osirion.org>

  ------------------------------------------------------------------
  
	2013-11-03

	This is a collection of skyboxes I created for my spacegame
	Project::OSiRiON. I've converted them to quake3 standards
	so they can used by level designers in quake3-derived engines.
	
	Originally, the skies were rendered at 2048x2048. Because
	neither Tremulous nor Unvanquished seemed to like this resolution
	I had to convert them to 1024x1024. The source XML files are included
	in this release: if required, you can always render them 
	to higher resolution images if you so desire.
	
	All the skyboxes were created with Alex Peterson's Spacescape:
	http://alexcpeterson.com/spacescape
	
	A tutorial I wrote can be found here:
	http://osirion.org/wiki/index.php/Creating_Skyboxes
	
	If you redestribute this files can do so under the terms and conditions
	of either the CREATIVE COMMONS ATTRIBUTION-SHARE ALIKE 3.0 LICENSE
	or the GENERAL PUBLIC LICENSE, version 2.

	Please refer to the following documents 
	for more information about these licenses.
	
		http://creativecommons.org/licenses/by-sa/3.0/
		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
	
	Visit the Project::OSiRiON.website
	http://osirion.org

  ------------------------------------------------------------------
  
	Changelog
	
	2013-11-03 Release 02
	
	- Added sky21 through sky30.
	- Updated sky01
	- Updated sky04
	- Updated sky05
	- Updated sky09
	- Updated sky10
	- Updated sky12
	- Converted release files to JPEG format.
	
	2012-12-14 Release 01
	
	- Initial public release
