#!/bin/sh

sky="$1"

convert_defaults="-quality 92"

convert ${sky}_back6.png ${convert_defaults} ${sky}_bk.jpg

# rotate 90 C
convert ${sky}_bottom4.png ${convert_defaults} -rotate 90 ${sky}_dn.jpg

convert ${sky}_front5.png ${convert_defaults} ${sky}_ft.jpg

convert ${sky}_left2.png ${convert_defaults} ${sky}_rt.jpg

convert ${sky}_right1.png ${convert_defaults} ${sky}_lf.jpg 

# rotate 90 CC
convert ${sky}_top3.png ${convert_defaults} -rotate 270 ${sky}_up.jpg

# create thumbnail
convert ${sky}_top3.png ${convert_defaults} -scale 256 ${sky}.jpg
