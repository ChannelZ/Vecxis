#!/bin/sh

find * -iname '*.png*' | parallel --gnu "optipng -o7 -zm1-9 {}"
