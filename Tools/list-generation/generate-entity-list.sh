#!/bin/env sh
rm entity-list.txt
for n in `find * -iname '*.def'`; do
grep '/*QUAKED' $n | sed 's/\/\*QUAKED\ //' | sed 's/[\x20-\x7E]*[ ].*//' >> entity-list.txt;
done
sort -u entity-list.txt -oentity-list.txt