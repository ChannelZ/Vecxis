#!/bin/env sh

for n in `find * -iname '*.*[ch]'`; do grep -e 'classname' $n | sed 's/^[ \t]*//' | sed 's/\(^.*"\)\(.*\)\(".*$\)/\2/p' | sed '/classname/d' | sed '/\/\//d' | sed '/\\n/d'; done | sort -u | sed -n '/[a-z]/p' > classnames-list.txt