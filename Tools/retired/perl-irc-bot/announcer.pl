# Copyright (c) 2008 Rudolf "divVerent" Polzer
# Copyright (C) 2012-2013 Micah Talkiewicz
# 
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

sub out($$@);

	# on game start, notify the channel
	[ dp => q{:gamestart:(.*):[0-9.]*} => sub {
		my ($map) = @_;
		$store{playing} = 1;
		$store{map} = $map;
		$store{map_starttime} = time();
		if ($config{irc_announce_mapchange} eq 'always' || ($config{irc_announce_mapchange} eq 'notempty' && $store{slots_active} > 0)) {
			my $slotsstr = nex_slotsstring();
			out irc => 0, "PRIVMSG $config{irc_channel} :\00304" . $map . "\017 has begun$slotsstr";
		}
		delete $store{lms_blocked};
		return 0;
	} ],

	# on game over, clear the current map
	[ dp => q{:gameover} => sub {
		$store{playing} = 0;
		return 0;
	} ],

	# scores: Nexuiz server -> IRC channel (start)
	[ dp => q{:scores:(.*):(\d+)} => sub {
		my ($map, $time) = @_;
		$store{scores} = {};
		$store{scores}{map} = $map;
		$store{scores}{time} = $time;
		$store{scores}{players} = [];
		delete $store{lms_blocked};
		return 0;
	} ],

	# scores: Nexuiz server -> IRC channel, legacy format
	[ dp => q{:player:(-?\d+):(\d+):(\d+):(\d+):(\d+):(.*)} => sub {
		my ($frags, $deaths, $time, $team, $id, $name) = @_;
		return if not exists $store{scores};
		push @{$store{scores}{players}}, [$frags, $team, $name]
			unless $frags <= -666; # no spectators
		return 0;
	} ],

	# scores: Nexuiz server -> IRC channel (CTF), legacy format
	[ dp => q{:teamscores:(\d+:-?\d*(?::\d+:-?\d*)*)} => sub {
		my ($teams) = @_;
		return if not exists $store{scores};
		$store{scores}{teams} = {split /:/, $teams};
		return 0;
	} ],

	# scores: Nexuiz server -> IRC channel, new format
	[ dp => q{:player:see-labels:(-?\d+)[-0-9,]*:(\d+):(\d+):(\d+):(.*)} => sub {
		my ($frags, $time, $team, $id, $name) = @_;
		return if not exists $store{scores};
		push @{$store{scores}{players}}, [$frags, $team, $name];
		return 0;
	} ],
	
	# scores: Nexuiz server -> IRC channel (CTF), new format
	[ dp => q{:teamscores:see-labels:(-?\d+)[-0-9,]*:(\d+)} => sub {
		my ($frags, $team) = @_;
		return if not exists $store{scores};
		$store{scores}{teams}{$team} = $frags;
		return 0;
	} ],

	[ dp => q{:ctf:capture:(\d+):(\d+)} => sub {
		my ($team, $id) = @_;
		my $nick = $store{"playernick_byid_$id"};
		if ($team eq "14")
		{
			out irc => 0, "PRIVMSG $config{irc_channel} :$nick has captured the \00312BLUE\017 flag";
		}
		else
		{
			out irc => 0, "PRIVMSG $config{irc_channel} :$nick has captured the \00304RED\017 flag";
		}

		return 0;
	} ],
	
	# scores: Nexuiz server -> IRC channel
	[ dp => q{:end} => sub {
		return if not exists $store{scores};
		my $s = $store{scores};
		delete $store{scores};
		my $teams_matter = defined $s->{teams};

		my @t = ();
		my @p = ();

		if($teams_matter)
		{
			# put players into teams
			my %t = ();
			for(@{$s->{players}})
			{
				my $thisteam = ($t{$_->[1]} ||= {score => 0, team => $_->[1], players => []});
				push @{$thisteam->{players}}, [$_->[0], $_->[1], $_->[2]];
				if($s->{teams})
				{
					$thisteam->{score} = $s->{teams}{$_->[1]};
				}
				else
				{
					$thisteam->{score} += $_->[0];
				}
			}

			# sort by team score
			@t = sort { $b->{score} <=> $a->{score} } values %t;

			# sort by player score
			@p = ();
			for(@t)
			{
				@{$_->{players}} = sort { $b->[0] <=> $a->[0] } @{$_->{players}};
				push @p, @{$_->{players}};
			}
		}
		else
		{
			@p = sort { $b->[0] <=> $a->[0] } @{$s->{players}};
		}

		# no display for empty server
		return 0
			if !@p;

		# make message fit somehow
		for my $maxnamelen(reverse 3..64)
		{
			my $scores_string = "PRIVMSG $config{irc_channel} :\00304" . $s->{map} . "\017 ended:";
			if($teams_matter)
			{
				my $sep = ' ';
				for(@t)
				{
					$scores_string .= $sep . "\003" . $color_team2irc_table{$_->{team}}. "\002\002" . $_->{score} . "\017";
					$sep = ':';
				}
			}
			my $sep = '';
			for(@p)
			{
				my ($frags, $team, $name) = @$_;
				$name = color_dpfix substr($name, 0, $maxnamelen);
				if($teams_matter)
				{
					$name = "\003" . $color_team2irc_table{$team} . " " . color_dp2none $name;
				}
				else
				{
					$name = " " . color_dp2irc $name;
				}
				$scores_string .= "$sep$name\017 $frags";
				$sep = ',';
			}
			if(length($scores_string) <= ($store{irc_maxlen} || 256))
			{
				out irc => 0, $scores_string;
				return 0;
			}
		}
		out irc => 0, "PRIVMSG $config{irc_channel} :\001ACTION would have LIKED to put the scores here, but they wouldn't fit :(\001";
		return 0;
	} ],

	# LMS: detect "no more lives" message
	[ dp => q{\^4.*\^4 has no more lives left} => sub {
		if(!$store{lms_blocked})
		{
			$store{lms_blocked} = 1;
			if(!$store{slots_full})
			{
				schedule sub {
					if($store{lms_blocked})
					{
						out irc => 0, "PRIVMSG $config{irc_channel} :\001ACTION can't be joined until next round (a player has no more lives left)\001";
					}
				} => 1;
			}
		}
	} ],