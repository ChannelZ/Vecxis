# Copyright (c) 2008 Rudolf "divVerent" Polzer
# Copyright (C) 2012 Micah Talkiewicz
# 
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

sub out($$@);

# IRC admin commands
	[ irc => q{:(([^! ]*)![^ ]*) (?i:PRIVMSG) [^&#%]\S* :(.*)} => sub {
		return 0 unless $config{irc_admin_password} ne '';

		my ($hostmask, $nick, $command) = @_;
		my $dpnick = color_dpfix $nick;

		# Nonprotected Commands. (usable by all)

		#suggest requires the server to be extended to support it.
		if($command =~ /^suggest (.*)$/)
		{
			my ($suggest) = ($1);
			out dp => 0, "sv_cmd suggest $suggest";
			out irc => 0, "PRIVMSG $nick :Map suggested.";
			#out irc => 0, "PRIVMSG $config{irc_channel} :Map suggested by $nick, $suggest.";
			return 0;
		}



		#Semi-Protected Comands. (these work differently depending on whether a person is logged in or not.)

		 if($command =~ /^help(?: (.*))?$/)
		 {
			my ($inquiry) = ($1);
			if($inquiry =~ "status")
			{
				out irc => 0, "PRIVMSG $nick : The \"status\" command displays information about the players on the server.";
				return 0;
			}
			if($inquiry =~ "suggest")
			{
				out irc => 0, "PRIVMSG $nick : The \"suggest\" command alows you to pick one or more maps for the end of match voting screen.";
				out irc => 0, "PRIVMSG $nick : Usage: suggest <required_mapname> <optional_mapname(s)>";
				return 0;
			}
			#admin commands (hidden from those who aren't logged in
			if(!(($store{logins}{$hostmask} || 0) < time()))
			{
				if($inquiry =~ "chmap")
				{
					out irc => 0, "PRIVMSG $nick : The \"chmap\" command alows you to force the server to switch the specified map.";
					out irc => 0, "PRIVMSG $nick : Usage: chmap <mapname>";
					return 0;
				}
				if($inquiry =~ "kick")
				{
					out irc => 0, "PRIVMSG $nick : The \"kick\" command kicks a player off the server. Don't forget to provide a reason. To find the player number use status.";
					out irc => 0, "PRIVMSG $nick : Usage: kick <player_number> <reason>";
					return 0;
				}
				if($inquiry =~ "mute")
				{
					out irc => 0, "PRIVMSG $nick : The \"mute\" command silences a player. To find the player number use status.";
					out irc => 0, "PRIVMSG $nick : Usage: mute <player_number>";
					return 0;
				}
				if($inquiry =~ "unmute")
				{
					out irc => 0, "PRIVMSG $nick : The \"unmute\" command unsilences a player. To find the player number use status.";
					out irc => 0, "PRIVMSG $nick : Usage: unmute <player_number>";
					return 0;
				}			
				if($inquiry =~ "tell")
				{
					out irc => 0, "PRIVMSG $nick : The \"tell\" command allows you to whisper to a player. To find the player number use status.";
					out irc => 0, "PRIVMSG $nick : Usage: tell <player_number> <message>";
					return 0;
				}
				if($inquiry =~ "bans")
				{
					out irc => 0, "PRIVMSG $nick : The \"bans\" command displays a list of banned players.";
					out irc => 0, "PRIVMSG $nick : Usage: bans";
					return 0;
				}
				if($inquiry =~ "restart")
				{
					out irc => 0, "PRIVMSG $nick : The \"restart\" command displays a warning, then restarts the match after the specified amount of seconds.";
					out irc => 0, "PRIVMSG $nick : Usage: restart <seconds_until_restart>";
					return 0;
				}
				if($inquiry eq "gravity")
				{
					out irc => 0, "PRIVMSG $nick : The \"gravity\" command allows you to set the force of gravity. The command \"gravity sane\" sets this to 800.";
					out irc => 0, "PRIVMSG $nick : Usage: gravity <value>";
					return 0;
				}
				if($inquiry eq "gravity sane")
				{
					out irc => 0, "PRIVMSG $nick : The \"gravity sane\" command resets gravity to a sane value.";
					out irc => 0, "PRIVMSG $nick : Usage: gravity sane>";
					return 0;
				}
				if($inquiry eq "gametype")
				{
					out irc => 0, "PRIVMSG $nick : The \"gametype\" command allows you to attempt to change the mode of game play. Use the letters in ( ) to select which to play.";
					out irc => 0, "PRIVMSG $nick : Modes are; death-match (dm), team death-match(tdm), capture-the-flag(ctf), domination(dom).";
					out irc => 0, "PRIVMSG $nick : Usage: gametype <mode>";
					return 0;
				}
				if($inquiry eq "timelimit")
				{
					out irc => 0, "PRIVMSG $nick : The \"timelimit\" command allows you to set the total amount of time the game is to be played. Note, \"time-remaining\" and \"timelimit\" are two different things.";
					out irc => 0, "PRIVMSG $nick : Usage: timelimit <minute(s)>";
					return 0;
				}
				if($inquiry eq "fs_rescan")
				{
					out irc => 0, "PRIVMSG $nick : The \"fs_rescan\" command checks for files newly added or removed fromthe server.";
					out irc => 0, "PRIVMSG $nick : Usage: fs_rescan";
					return 0;
				}
				if($inquiry eq "minsta")
				{
					out irc => 0, "PRIVMSG $nick : The \"minsta\" command sets the game mode to minsta, then does the required restart.";
					out irc => 0, "PRIVMSG $nick : Usage: minsta";
					return 0;
				}
				if($inquiry eq "weapons")
				{
					out irc => 0, "PRIVMSG $nick : The \"weapons\" command sets the game mode to weapons, then does the required restart.";
					out irc => 0, "PRIVMSG $nick : Usage: weapons";
					return 0;
				}
				if($inquiry =~ "list")
				{
				out irc => 0, "PRIVMSG $nick : Available commands are; help, fs_rescan, gametype, gravity, gravity sane, suggest, status, timelimit, mute, unmute, chmap, tell, kick, bans, restart, weapons, minsta.";
				return 0;
				}
			} else {
				if($inquiry =~ "list")
				{
				out irc => 0, "PRIVMSG $nick : Available commands are; help, suggest, status.";
				return 0;
				}
			}
			out irc => 0, "PRIVMSG $nick : Type \"help <command>\" or, type \"help list\" for a list of commands.";
			return 0;
		 }


		if($command =~ /^status(?: (.*))?$/)
		{
			my ($match) = $1;
			my $found = 0;
			my $foundany = 0;
			for my $slot(@{$store{playerslots_active} || []})
			{
				my $s = $store{"playerslot_$slot"};
				next unless $s;
				if(not defined $match or index(color_dp2none($s->{name}), $match) >= 0)
				{ if(!($s->{ip} =~ "botclient")){#disable displaying of bot status
					if((($store{logins}{$hostmask} || 0) < time()))
					{
						if($s->{ip} =~ "botclient")
						{
							out irc => 0, sprintf 'PRIVMSG %s :%-21s %2i %4i %8s %4i #%-3u %s', $nick, $s->{ip}, $s->{pl}, $s->{ping}, $s->{time}, $s->{frags}, $slot, color_dp2irc $s->{name};
						}
						 else
						{
							out irc => 0, sprintf 'PRIVMSG %s :<N/A> %2i %4i %8s %4i #%-3u %s', $nick, $s->{pl}, $s->{ping}, $s->{time}, $s->{frags}, $slot, color_dp2irc $s->{name};
						}
					}
					else
					{
						out irc => 0, sprintf 'PRIVMSG %s :%-21s %2i %4i %8s %4i #%-3u %s', $nick, $s->{ip}, $s->{pl}, $s->{ping}, $s->{time}, $s->{frags}, $slot, color_dp2irc $s->{name};
					}}
				++$found;
				}
				++$foundany;
			}
			if(!$found)
			{
				if(!$foundany)
				{
					out irc => 0, "PRIVMSG $nick :the server is empty";
				}
				else
				{
					out irc => 0, "PRIVMSG $nick :no nicknames match";
				}
			}
			return 0;
		}

		#Login system
		#TODO: add support for diffrent users/passwords
		if($command eq "Login: $config{irc_admin_password}")
		{
			$store{logins}{$hostmask} = time() + $config{irc_admin_timeout};
			out irc => 0, "PRIVMSG $nick :authenticated";
			return -1;
		}

		if($command =~ /^Login: /)
		{
			out irc => 0, "PRIVMSG $nick :invalid password";
			out irc => 0, "PRIVMSG $config{irc_channel} :Warning! $nick attemped to login.";
			return -1;
		}

		#Protected Comands. (only for users who are logged in)
		if(!(($store{logins}{$hostmask} || 0) < time()))
		{

		if($command =~ /^kick # (\d+) (.*)$/)
		{
			my ($id, $reason) = ($1, $2);
			my $dpreason = color_irc2dp $reason;
			$dpreason =~ s/^(~?)(.*)/$1irc $dpnick: $2/g;
			$dpreason =~ s/(["\\])/\\$1/g;
			out dp => 0, "kick # $id $dpreason";
			my $slotnik = "playerslot_$id";
			out irc => 0, "PRIVMSG $nick :kicked #$id (@{[color_dp2irc $store{$slotnik}{name}]}\017 @ $store{$slotnik}{ip}) ($reason)";
			return 0;
		}

		if($command =~ /^kickban # (\d+) (\d+) (\d+) (.*)$/)
		{
			my ($id, $bantime, $mask, $reason) = ($1, $2, $3, $4);
			my $dpreason = color_irc2dp $reason;
			$dpreason =~ s/^(~?)(.*)/$1irc $dpnick: $2/g;
			$dpreason =~ s/(["\\])/\\$1/g;
			out dp => 0, "kickban # $id $bantime $mask $dpreason";
			my $slotnik = "playerslot_$id";
			out irc => 0, "PRIVMSG $nick :kickbanned #$id (@{[color_dp2irc $store{$slotnik}{name}]}\017 @ $store{$slotnik}{ip}), netmask $mask, for $bantime seconds ($reason)";
			return 0;
		}

		if($command eq "bans")
		{
			my $banlist =
				join ", ",
				map { "$_ ($store{bans}[$_]{ip}, $store{bans}[$_]{time}s)" }
				0..@{$store{bans} || []}-1;
			$banlist = "no bans"
				if $banlist eq "";
			out irc => 0, "PRIVMSG $nick :$banlist";
			return 0;
		}

		if($command =~ /^unban (\d+)$/)
		{
			my ($id) = ($1);
			out dp => 0, "unban $id";
			out irc => 0, "PRIVMSG $nick :removed ban $id ($store{bans}[$id]{ip})";
			return 0;
		}

		if($command =~ /^mute (\d+)$/)
		{
			my $id = $1;
			out dp => 0, "mute $id";
			my $slotnik = "playerslot_$id";
			out irc => 0, "PRIVMSG $nick :muted $id (@{[color_dp2irc $store{$slotnik}{name}]}\017 @ $store{$slotnik}{ip})";
			return 0;
		}

		if($command =~ /^unmute (\d+)$/)
		{
			my ($id) = ($1);
			out dp => 0, "unmute $id";
			my $slotnik = "playerslot_$id";
			out irc => 0, "PRIVMSG $nick :unmuted $id (@{[color_dp2irc $store{$slotnik}{name}]}\017 @ $store{$slotnik}{ip})";
			return 0;
		}
		if($command =~ /^chmap (.*)$/)
		{
			my ($chmap) = ($1);
			out irc => 0, "PRIVMSG $nick :changing to map $chmap";
			out dp => 0, "defer 15 \"chmap $chmap\"";
			out dp => 0, "say ^6 Alert: ^x99f changing to map ^7\"$chmap\"^x99f in 15 seconds!";
			return 0;
		}
		if($command =~ /^tell (\d+) (.*)$/)
		{
			my ($buddy) = ($1);
			my ($whisper) = ($2);
			out dp => 0, "rcon2irc_tell_as $nick $buddy $whisper";
			return 0;
		}
		if($command eq "restart")
		{
			out irc => 0, "PRIVMSG $nick :Please specify a time until restart.";
			out irc => 0, "PRIVMSG $nick : Usage: restart <seconds_until_restart>";
			return 0;
		}
		if($command =~ /^restart (\d+)$/)
		{
			my ($defer) = ($1);
			#this may not need to be on two lines
			out dp => 0, "say ^1 WARNING: ^x99f Restart in $1 second(s)!";
			out dp => 0, "defer $1 restart";

			return 0;
		}
		if($command =~ /gravity (\d+)$/)
		{
			my ($newgravity) = ($1);
			out irc => 0, "PRIVMSG $nick :Gravity is now set at $newgravity.";
			out irc => 0, "PRIVMSG $config{irc_channel} :Gravity set by $nick.";
			out dp => 0, "set sv_gravity $newgravity";
			out dp => 0, "say ^xb5f[ Gravity is now set to ^xfff $newgravity ^xb5f. ]";
			return 0;
		}
		if($command eq "gravity sane")
		{
			out irc => 0, "PRIVMSG $nick :Gravity will return to normal in 15 seconds.";
			out irc => 0, "PRIVMSG $config{irc_channel} :Gravity reset by $nick.";
			out dp => 0, "defer 15 \"set sv_gravity 800\"";
			out dp => 0, "say ^xb5f[ Gravity will return to normal in ^xfff 15 ^xb5f seconds. ]";
			return 0;
		}
		if($command =~ /timelimit (\d+)$/)
		{
			my ($newtimelimit) = ($1);
			#lets keep time limits safe
			if($newtimelimit > 60 ){$newtimelimit = 60;}elsif($newtimelimit < 1){$newtimelimit = 1;}

			out irc => 0, "PRIVMSG $nick :Timelimit is now set at $newtimelimit.";
			out irc => 0, "PRIVMSG $config{irc_channel} :Timelimit set by $newtimelimit.";
			out dp => 0, "set timelimit $newtimelimit";
			out dp => 0, "say ^xb5f[ Timelimit is now set to ^xfff $newtimelimit ^xb5f minute(s). ]";
			
			return 0;
		}
		if($command eq "fs_rescan")
		{
			out dp => 0, "fs_rescan";
			out irc => 0, "PRIVMSG $nick :Rescaning...";
			return 0;
		}
		if($command eq "minsta")
		{
			out dp => 0, "say ^6 Alert: ^x99f switching to Minstagib in 15 seconds!";
			out dp => 0, "g_minstagib 1";
			out dp => 0, "defer 15 restart";
			out irc => 0, "PRIVMSG $nick :Switching...";
			out irc => 0, "PRIVMSG $config{irc_channel} :Switching to Minstagib!";
			return 0;
		}
		if($command eq "weapons")
		{
			out dp => 0, "say ^6 Alert: ^x99f switching to Weapons in 15 seconds!";
			out dp => 0, "g_minstagib 0";
			out dp => 0, "defer 15 restart";
			out irc => 0, "PRIVMSG $nick :Switching...";
			out irc => 0, "PRIVMSG $config{irc_channel} :Switching to Weapons mode!.";
			return 0;
		}

		if($command =~ /^log_dest_udp (.*)$/)
		{
			my ($log_dest_from_wan) = ($1);
			out dp => 0, "log_dest_udp $log_dest_from_wan";
			out irc => 0, "PRIVMSG $nick :Changing log destonation to $log_dest_from_wan";
			return 0;
		}

		
		#==============[probably dangerous]================
		#if($command =~ /^quote (.*)$/)
		#{
		#	my ($cmd) = ($1);
		#	if($cmd =~ /^(??{$config{irc_admin_quote_re}})$/si)
		#	{
		#		out irc => 0, $cmd;
		#		out irc => 0, "PRIVMSG $nick :executed your command";
		#	}
		#	else
		#	{
		#		out irc => 0, "PRIVMSG $nick :permission denied";
		#	}
		#	return 0;
		#}

		}

		#fall-back message, in case we have invalid commands (or the user entered a valid command but was not logged in).
		out irc => 0, "PRIVMSG $nick : Please consider typing \"help\".";
		return -1;
	} ],