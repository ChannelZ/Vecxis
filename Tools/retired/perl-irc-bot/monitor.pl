# Copyright (c) 2008 Rudolf "divVerent" Polzer
# Copyright (C) 2012-2013 Micah Talkiewicz
# 
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

sub out($$@);


	# chat: Nexuiz server -> IRC channel, nick change/set
	[ dp => q{:name:(\d+):(.*)} => sub {
		my ($id, $nick) = @_;
		$store{"playernickraw_byid_$id"} = $nick;
		$nick = color_dp2irc $nick;
		my $oldnick = $store{"playernick_byid_$id"};
		out irc => 0, "PRIVMSG $config{irc_channel} :* $oldnick\017 is now known as $nick";
		$store{"playernick_byid_$id"} = $nick;
		return 0;
	} ],

	# complain when system load gets too high
	[ dp => q{timing:   (([0-9.]*)% CPU, ([0-9.]*)% lost, offset avg ([0-9.]*)ms, max ([0-9.]*)ms, sdev ([0-9.]*)ms)} => sub {
		my ($all, $cpu, $lost, $avg, $max, $sdev) = @_;
		return 0 # don't complain when just on the voting screen
			if !$store{playing};
		return 0 # don't complain if it was less than 0.5%
			if $lost < 0.5;
		return 0 # don't complain if nobody is looking
			if $store{slots_active} == 0;
		return 0 # don't complain in the first two minutes
			if time() - $store{map_starttime} < 120;
		return 0 # don't complain if it was already at least half as bad in this round
			if $store{map_starttime} == $store{timingerror_map_starttime} and $lost <= 2 * $store{timingerror_lost};
		$store{timingerror_map_starttime} = $store{map_starttime};
		$store{timingerror_lost} = $lost;
		out dp => 0, 'rcon2irc_say_as server "There are currently some severe system load problems. The admins have been notified."';
		out irc => 1, "PRIVMSG $config{irc_channel} :\001ACTION has big trouble on $store{map} after @{[int(time() - $store{map_starttime})]}s: $all\001";
		#out irc => 1, "PRIVMSG OpBaI :\001ACTION has big trouble on $store{map} after @{[int(time() - $store{map_starttime})]}s: $all\001";
		return 0;
	} ],
