

# Copyright (c) 2008 Rudolf "divVerent" Polzer
# 
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

sub out($$@);

# chat: Nexuiz server -> IRC channel, vote call
	[ dp => q{:vote:vcall:(\d+):(.*)} => sub {
		my ($id, $command) = @_;
		$command = color_dp2irc $command;
		my $oldnick = $id ? $store{"playernick_byid_$id"} : "(console)";
		out irc => 0, "PRIVMSG $config{irc_channel} :* $oldnick\017 calls a vote for \"$command\017\"";
		return 0;
	} ],

	# chat: Nexuiz server -> IRC channel, vote stop
	[ dp => q{:vote:vstop:(\d+)} => sub {
		my ($id) = @_;
		my $oldnick = $id ? $store{"playernick_byid_$id"} : "(console)";
		out irc => 0, "PRIVMSG $config{irc_channel} :* $oldnick\017 stopped the vote";
		return 0;
	} ],

	# chat: Nexuiz server -> IRC channel, master login
	[ dp => q{:vote:vlogin:(\d+)} => sub {
		my ($id) = @_;
		my $oldnick = $id ? $store{"playernick_byid_$id"} : "(console)";
		out irc => 0, "PRIVMSG $config{irc_channel} :* $oldnick\017 logged in as master";
		return 0;
	} ],

	# chat: Nexuiz server -> IRC channel, master do
	[ dp => q{:vote:vdo:(\d+):(.*)} => sub {
		my ($id, $command) = @_;
		$command = color_dp2irc $command;
		my $oldnick = $id ? $store{"playernick_byid_$id"} : "(console)";
		out irc => 0, "PRIVMSG $config{irc_channel} :* $oldnick\017 used master status to do \"$command\017\"";
		return 0;
	} ],

	# chat: Nexuiz server -> IRC channel, result
	[ dp => q{:vote:v(yes|no|timeout):(\d+):(\d+):(\d+):(\d+):(-?\d+)} => sub {
		my ($result, $yes, $no, $abstain, $not, $min) = @_;
		my $spam = "$yes:$no" . (($min >= 0) ? " ($min needed)" : "") . ", $abstain didn't care, $not didn't vote";
		out irc => 0, "PRIVMSG $config{irc_channel} :* the vote ended with $result: $spam";
		return 0;
	} ], 
