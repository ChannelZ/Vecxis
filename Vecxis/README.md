Welcome to Vecxis(TM)!


1.) About.
2.) License.
3.) Build notes.
4.) Notes on DarkPlaces.
5.) HELP! (wiki and community forums)
6.) Credits

1: Vecxis is a Free, Libre, Open Source Shooter. It is currently in its beta stage and is subject to many bugs and changes.
	You can get more information and a copy of Vecxis at http://www.vecxis.com

2: The game and content are released under the terms of the GNU GPL version 2+ unless otherwise noted. Some content is dual licensed. Some content, including the portions of the soundtrack, are licensed under the CC-BY-SA. These licenses do not apply to the use of the Vecxis trademark, which is owned by Micah Talkiewicz.

3: Build notes:
	A.) Yes I know the build system functionally retarded.
	B.) There are some other tools as well as the main source repository that can be found here: http://projects.gamebuf.com/Vecxis/
	C.) Dependencies are not currently documented. A short (and probably incomplete) list includes ImageMagick, nasm, ogg-tools, 7zip.

4: Vecxis uses a very slightly modified version of Akari's DarkPlacesRM. His code can be found here: https://github.com/nexAkari/DarkPlacesRM
I would also like to thank LordHavoc for continuing to support his engine over the years. He is not involved in Vecxis however I feel the need to include a link to his page at http://icculus.org/twilight/darkplaces/

5: Please check online for more help. There is a wiki in addition to community forums http://www.vecxis.com/wiki ,  http://forums.vecxis.com .
If you *really* need help, you can *try* emailing me @ p2_ at mail.com Or Player_2 at vecxis.org. I rarely check my email, therefor I will likely be very slow (if at all) to respond.

6: Credits are still being officially compiled, so this is more of a thank you to every one who has helped with Vecxis.
	There is also a list of credits taken from Nexuiz, which Vecxis was forked from. This file is located in the Documents folder.