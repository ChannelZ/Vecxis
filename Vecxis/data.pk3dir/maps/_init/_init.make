RMDIR?= rmdir
MAPC?= q3map2

MAPNAME?= _init/_init
MAPCOPTS?= -game vecxis -fs_basepath ../../ -fs_game data
BSPOPTS?= -meta -samplesize 8 -mv 1000000 -mi 6000000

all: bsp

bsp:
	$(MAPC) $(MAPCOPTS) $(BSPOPTS) $(MAPNAME).map
