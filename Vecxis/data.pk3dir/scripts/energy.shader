 
textures/energy/Beam-001-Blue
{
	qer_trans 0.35
	q3map_surfacelight 7500
	
	
	//surfaceparm alphashadow
	surfaceparm trans
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm nodlight
	surfaceparm nolightmap
	
	cull none
	//polygonOffset
	
	DeformVertexes autosprite2
	
	{
	map textures/energy/Beam-001-Blue.png
	tcGen base
	tcMod scroll 1.25 0
	blendFunc GL_ONE GL_ONE_MINUS_SRC_ALPHA
	}

}

textures/energy/Beam-001-Red
{
	qer_trans 0.35
	q3map_surfacelight 7500
	
	
	//surfaceparm alphashadow
	surfaceparm trans
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm nodlight
	surfaceparm nolightmap
	
	cull none
	//polygonOffset
	
	DeformVertexes autosprite2
	
	{
	map textures/energy/Beam-001-Red.png
	tcGen base
	tcMod scroll 1.25 0
	blendFunc GL_ONE GL_ONE_MINUS_SRC_ALPHA
	}

}

textures/energy/Beam-001-Bright-Blue
{
	qer_trans 0.35
	q3map_surfacelight 50000
	
	
	//surfaceparm alphashadow
	surfaceparm trans
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm nodlight
	surfaceparm nolightmap
	
	cull none
	//polygonOffset
	
	DeformVertexes autosprite2
	
	{
	map textures/energy/Beam-001-Blue.png
	tcGen base
	tcMod scroll 1.25 0
	blendFunc GL_ONE GL_ONE_MINUS_SRC_ALPHA
	}

}

textures/energy/Beam-001-Bright-Red
{
	qer_trans 0.35
	q3map_surfacelight 50000
	
	
	//surfaceparm alphashadow
	surfaceparm trans
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm nodlight
	surfaceparm nolightmap
	
	cull none
	//polygonOffset
	
	DeformVertexes autosprite2
	
	{
	map textures/energy/Beam-001-Red.png
	tcGen base
	tcMod scroll 1.25 0
	blendFunc GL_ONE GL_ONE_MINUS_SRC_ALPHA
	}

}

textures/energy/Ball-001-Blue
{
	qer_trans 0.35
	q3map_surfacelight 50000
	
	
	//surfaceparm alphashadow
	surfaceparm trans
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm nodlight
	surfaceparm nolightmap
	
	cull none
	//polygonOffset
	
	DeformVertexes autosprite
	
	{
	map textures/energy/Ball-001-Red.png
	rgbGen wave Sin .5 .2 1 2
	blendFunc add GL_ONE GL_ONE
	map textures/energy/Ball-001-Blue.png
	tcGen base
	//tcMod rotate 3
	//tcMod turb 1 0.05 1 1
	//tcMod scroll 1.25 0
	blendFunc GL_ONE GL_ONE_MINUS_SRC_ALPHA
	}

}

textures/energy/Ball-001-Red
{
	qer_trans 0.35
	q3map_surfacelight 50000
	
	
	//surfaceparm alphashadow
	surfaceparm trans
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm nodlight
	surfaceparm nolightmap
	
	cull none
	//polygonOffset
	
	DeformVertexes autosprite
	
	{
	map textures/energy/Ball-001-Red.png
	rgbGen wave Sin .5 .2 1 2
	blendFunc add GL_ONE GL_ONE
	tcGen base
	//tcMod rotate 3
	//tcMod turb 1 0.05 1 3
	//tcMod scroll 1.25 0
	blendFunc GL_ONE GL_ONE_MINUS_SRC_ALPHA
	}

}