textures/flynn_light/neon-blue-1000
{
	qer_editorimage textures/flynn_light/neon-blue1.tga
	//q3map_lightSubdivide 4
	q3map_lightImage textures/flynn_light/neon-blue1.tga
	q3map_surfacelight 1000
	{
		map textures/flynn_light/neon-blue1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/flynn_light/neon-red-1000
{
	qer_editorimage textures/flynn_light/neon-red1.tga
	//q3map_lightSubdivide 4
	q3map_lightImage textures/flynn_light/neon-red1.tga
	q3map_surfacelight 1000
	{
		map textures/flynn_light/neon-red1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/flynn_light/neon-blue-2000
{
	qer_editorimage textures/flynn_light/neon-blue1.tga
	//q3map_lightSubdivide 4
	q3map_lightImage textures/flynn_light/neon-blue1.tga
	q3map_surfacelight 2000
	{
		map textures/flynn_light/neon-blue1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/flynn_light/neon-red-2000
{
	qer_editorimage textures/flynn_light/neon-red1.tga
	//q3map_lightSubdivide 4
	q3map_lightImage textures/flynn_light/neon-red1.tga
	q3map_surfacelight 2000
	{
		map textures/flynn_light/neon-red1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/flynn_light/neon-blue-3000
{
	qer_editorimage textures/flynn_light/neon-blue1.tga
	//q3map_lightSubdivide 4
	q3map_lightImage textures/flynn_light/neon-blue1.tga
	q3map_surfacelight 3000
	{
		map textures/flynn_light/neon-blue1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/flynn_light/neon-red-3000
{
	qer_editorimage textures/flynn_light/neon-red1.tga
	//q3map_lightSubdivide 4
	q3map_lightImage textures/flynn_light/neon-red1.tga
	q3map_surfacelight 3000
	{
		map textures/flynn_light/neon-red1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/flynn_light/neon-blue-4000
{
	qer_editorimage textures/flynn_light/neon-blue1.tga
	//q3map_lightSubdivide 4
	q3map_lightImage textures/flynn_light/neon-blue1.tga
	q3map_surfacelight 4000
	{
		map textures/flynn_light/neon-blue1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/flynn_light/neon-red-4000
{
	qer_editorimage textures/flynn_light/neon-red1.tga
	//q3map_lightSubdivide 4
	q3map_lightImage textures/flynn_light/neon-red1.tga
	q3map_surfacelight 4000
	{
		map textures/flynn_light/neon-red1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/flynn_light/neon-blue-5000
{
	qer_editorimage textures/flynn_light/neon-blue1.tga
	//q3map_lightSubdivide 4
	q3map_lightImage textures/flynn_light/neon-blue1.tga
	q3map_surfacelight 5000
	{
		map textures/flynn_light/neon-blue1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/flynn_light/neon-red-5000
{
	qer_editorimage textures/flynn_light/neon-red1.tga
	//q3map_lightSubdivide 4
	q3map_lightImage textures/flynn_light/neon-red1.tga
	q3map_surfacelight 5000
	{
		map textures/flynn_light/neon-red1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

// normal light without lightSubdivide

textures/flynn_light/neon-blue-bulb
{
	qer_editorimage textures/flynn_light/neon-blue1.tga
	q3map_lightImage textures/flynn_light/neon-blue1.tga
	q3map_surfacelight 20000
	{
		map textures/flynn_light/neon-blue1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/flynn_light/neon-red-bulb
{
	qer_editorimage textures/flynn_light/neon-red1.tga
	q3map_lightImage textures/flynn_light/neon-red1.tga
	q3map_surfacelight 20000
	{
		map textures/flynn_light/neon-red1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

// bottom side

textures/flynn_light/neon-blue-bottom-6000
{
	qer_editorimage textures/flynn_light/neon-blue1.tga
	//q3map_lightSubdivide 4
	q3map_lightImage textures/flynn_light/neon-blue1.tga
	q3map_surfacelight 10000
	{
		map textures/flynn_light/neon-blue1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/flynn_light/neon-red-bottom-6000
{
	qer_editorimage textures/flynn_light/neon-red1.tga
	//q3map_lightSubdivide 4
	q3map_lightImage textures/flynn_light/neon-red1.tga
	q3map_surfacelight 6000
	{
		map textures/flynn_light/neon-red1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/flynn_light/neon-blue-bottom-10000
{
	qer_editorimage textures/flynn_light/neon-blue1.tga
	//q3map_lightSubdivide 4
	q3map_lightImage textures/flynn_light/neon-blue1.tga
	q3map_surfacelight 10000
	{
		map textures/flynn_light/neon-blue1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/flynn_light/neon-red-bottom-10000
{
	qer_editorimage textures/flynn_light/neon-red1.tga
	//q3map_lightSubdivide 4
	q3map_lightImage textures/flynn_light/neon-red1.tga
	q3map_surfacelight 10000
	{
		map textures/flynn_light/neon-red1.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

// light beam

textures/flynn_light/neon-blue-beam
{
	qer_editorimage textures/flynn_light/neon-blue2.tga
	//q3map_lightSubdivide 32
	q3map_lightImage textures/flynn_light/neon-blue2.tga
	q3map_surfacelight 6000
	surfaceparm nonsolid
	{
		map textures/flynn_light/neon-blue2.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/flynn_light/neon-red-beam
{
	qer_editorimage textures/flynn_light/neon-red2.tga
	//q3map_lightSubdivide 32
	q3map_lightImage textures/flynn_light/neon-red2.tga
	q3map_surfacelight 6000
	surfaceparm nonsolid
	{
		map textures/flynn_light/neon-red2.tga
	}
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/flynn_base/e6grtfloorceil_s_cull
{
	qer_editorimage textures/evil6_floors/e6grtfloorceil.tga
	surfaceparm metalsteps
	{
		map textures/evil6_floors/e6grtfloorceil.tga
	}
	{
		map $lightmap
		rgbGen identity
	}
}
