textures/glass/crystal_blue
{
	qer_trans 0.20
	surfacepram alphashadow
	surfacepram trans


	{
	map textures/glass/crystal_blue.png
	blendFunc GL_ONE GL_SRC_ALPHA
	}
	//dp_reflect .1 .5 .5 .5 .9
}



textures/glass/crystal_green
{
	qer_trans 0.20
	surfacepram alphashadow
	surfacepram trans


	{
	map textures/glass/crystal_green.png
	blendFunc GL_ONE GL_SRC_ALPHA
	}
	//dp_reflect .1 .5 .5 .5 .9
}



textures/glass/crystal_red
{
	qer_trans 0.20
	surfacepram alphashadow
	surfacepram trans


	{
	map textures/glass/crystal_red.png
	blendFunc GL_ONE GL_SRC_ALPHA
	}
	//dp_reflect .1 .5 .5 .5 .9
}



textures/glass/crystal_purple
{
	qer_trans 0.20
	surfacepram alphashadow
	surfacepram trans


	{
	map textures/glass/crystal_purple.png
	blendFunc GL_ONE GL_SRC_ALPHA
	}
	//dp_reflect .1 .5 .5 .5 .9
}



textures/glass/crystal_yellow
{
	qer_trans 0.20
	surfacepram alphashadow
	surfacepram trans


	{
	map textures/glass/crystal_yellow.png
	blendFunc GL_ONE GL_SRC_ALPHA
	}
	//dp_reflect .9 .5 .5 .5 .9
}

textures/glass/glass_blue
{
	qer_editorimage textures/glass/glass_blue.tga
	surfaceparm trans
	cull disable
	qer_trans 0.5

	{
		map textures/glass/glass_blue.tga
		blendfunc add
		tcgen environment
		tcmod scale 4 4
	}	
}

textures/glass/glass_red
{
	qer_editorimage textures/glass/glass_red.tga
	surfaceparm trans
	cull disable
	qer_trans 0.5

	{
		map textures/glass/glass_red.tga
		blendfunc add
		tcgen environment
		tcmod scale 4 4
	}	
}

textures/glass/glass_green
{
	qer_editorimage textures/glass/glass_green.tga
	surfaceparm trans
	cull disable
	qer_trans 0.5

	{
		map textures/glass/glass_green.tga
		blendfunc add
		tcgen environment
		tcmod scale 4 4
	}	
}

textures/glass/glass_yellow
{
	qer_editorimage textures/glass/glass_yellow.tga
	surfaceparm trans
	cull disable
	qer_trans 0.5

	{
		map textures/glass/glass_yellow.tga
		blendfunc add
		tcgen environment
		tcmod scale 4 4
	}	
}

textures/glass/glass_purple
{
	qer_editorimage textures/glass/glass_purple.tga
	surfaceparm trans
	cull disable
	qer_trans 0.5

	{
		map textures/glass/glass_purple.tga
		blendfunc add
		tcgen environment
		tcmod scale 4 4
	}	
}

textures/glass/glass_dark
{
	qer_editorimage textures/glass/glass_dark.png
	surfaceparm trans
	cull disable
	qer_trans 0.5

	{
		map textures/glass/glass_dark.png
		blendFunc GL_ONE GL_SRC_ALPHA
		tcgen environment
		tcmod scale 4 4
	}	
}