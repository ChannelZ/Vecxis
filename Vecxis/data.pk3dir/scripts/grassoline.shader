textures/grassoline/hangwires
{
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm trans
	cull disable
	{
		map textures/grassoline/hangwires.tga
		alphaFunc GE128
	}
}
