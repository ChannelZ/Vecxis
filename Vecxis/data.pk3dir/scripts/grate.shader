 
textures/grate/grate-001
{
	qer_trans 0.40
	//surfacepram aphashadow
	surfacepram trans
	surfacepram nonsolid
	surfacepram playerclip
	surfacepram missleclip
	surfacepram detail
	surfacepram metalsteps
	surfacepram nopicmip
	//surfacepram nodamage
	//surfacepram nomipmap
	
	cull disable
	
	
	{
		map textures/grate/grate-001.png
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
		depthFunc equal
		depthWrite
	}
}
