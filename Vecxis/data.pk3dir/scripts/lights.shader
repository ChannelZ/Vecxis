 
textures/lights/light-001
{
	q3map_surfacelight 650
	q3map_lightImage textures/lights/lights-001_glow.jpg

	{
		map textures/lights/light-001.jpg
	}
	
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/lights/light-001-blue
{
	q3map_surfacelight 650
	q3map_lightImage textures/lights/lights-001-blue_glow.jpg

	{
		map textures/lights/light-001-blue.jpg
	}
	
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}

textures/lights/light-001-red
{
	q3map_surfacelight 650
	q3map_lightImage textures/lights/lights-001-red_glow.jpg

	{
		map textures/lights/light-001-red.jpg
	}
	
	{
		map $lightmap
		blendfunc filter
		tcGen lightmap
	}
}