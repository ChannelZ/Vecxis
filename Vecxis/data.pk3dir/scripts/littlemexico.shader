

textures/littlemexico/doorbig
{
        qer_editorimage textures/littlemexico/doorbig.tga

        surfaceparm alphashadow
        surfaceparm nomarks
        surfaceparm trans
        cull disable
        nopicmip

        {
                map textures/littlemexico/doorbig.tga
                rgbGen identity
                depthWrite
                blendFunc Blend  
        }
        {
                map $lightmap 
                blendfunc filter
                rgbGen identity
                tcGen lightmap 
                depthFunc equal
        }
}

textures/littlemexico/shutter1
{
        qer_editorimage textures/littlemexico/shutter1.tga

        surfaceparm alphashadow
        surfaceparm nomarks
        surfaceparm trans
        cull disable
        nopicmip

        {
                map textures/littlemexico/shutter1.tga
                rgbGen identity
                depthWrite
                blendFunc Blend  
        }
        {
                map $lightmap 
                blendfunc filter
                rgbGen identity
                tcGen lightmap 
                depthFunc equal
        }
}

textures/littlemexico/winshutter1
{
        qer_editorimage textures/littlemexico/winshutter1.tga

        surfaceparm alphashadow
        surfaceparm nomarks
        surfaceparm trans
        cull disable
        nopicmip

        {
                map textures/littlemexico/winshutter1.tga
                rgbGen identity
                depthWrite
                blendFunc Blend  
        }
        {
                map $lightmap 
                blendfunc filter
                rgbGen identity
                tcGen lightmap 
                depthFunc equal
        }
}

textures/littlemexico/shutter2
{
        qer_editorimage textures/littlemexico/shutter2.tga

        surfaceparm alphashadow
        surfaceparm nomarks
        surfaceparm trans
        cull disable
        nopicmip

        {
                map textures/littlemexico/shutter2.tga
                rgbGen identity
                depthWrite
                blendFunc Blend  
        }
        {
                map $lightmap 
                blendfunc filter
                rgbGen identity
                tcGen lightmap 
                depthFunc equal
        }
}

textures/littlemexico/iron
{
        qer_editorimage textures/littlemexico/iron.tga

        surfaceparm alphashadow
        surfaceparm nomarks
        surfaceparm trans
        cull disable
        nopicmip

        {
                map textures/littlemexico/iron.tga
                rgbGen identity
                depthWrite
                blendFunc Blend  
        }
        {
                map $lightmap 
                blendfunc filter
                rgbGen identity
                tcGen lightmap 
                depthFunc equal
        }
}

textures/littlemexico/window2
{
        qer_editorimage textures/littlemexico/window2.tga

        surfaceparm alphashadow
        surfaceparm nomarks
        surfaceparm trans
        cull disable
        nopicmip

        {
                map textures/littlemexico/window2.tga
                rgbGen identity
                depthWrite
                blendFunc Blend  
        }
        {
                map $lightmap 
                blendfunc filter
                rgbGen identity
                tcGen lightmap 
                depthFunc equal
        }
}

textures/littlemexico/doororange
{
        qer_editorimage textures/littlemexico/doororange.tga

        surfaceparm alphashadow
        surfaceparm nomarks
        surfaceparm trans
        cull disable
        nopicmip

        {
                map textures/littlemexico/doororange.tga
                rgbGen identity
                depthWrite
                blendFunc Blend  
        }
        {
                map $lightmap 
                blendfunc filter
                rgbGen identity
                tcGen lightmap 
                depthFunc equal
        }
}
textures/littlemexico/
{
        qer_editorimage textures/littlemexico/.tga

        surfaceparm alphashadow
        surfaceparm nomarks
        surfaceparm trans
        cull disable
        nopicmip

        {
                map textures/littlemexico/.tga
                rgbGen identity
                depthWrite
                blendFunc Blend  
        }
        {
                map $lightmap 
                blendfunc filter
                rgbGen identity
                tcGen lightmap 
                depthFunc equal
        }
}

textures/littlemexico/doorway1
{
        qer_editorimage textures/littlemexico/doorway1.tga

        surfaceparm alphashadow
        surfaceparm nomarks
        surfaceparm trans
        cull disable
        nopicmip

        {
                map textures/littlemexico/doorway1.tga
                rgbGen identity
                depthWrite
                blendFunc Blend  
        }
        {
                map $lightmap 
                blendfunc filter
                rgbGen identity
                tcGen lightmap 
                depthFunc equal
        }
}


textures/littlemexico/window3
{
        qer_editorimage textures/littlemexico/window3.tga

        surfaceparm alphashadow
        surfaceparm nomarks
        surfaceparm trans
        cull disable
        nopicmip

        {
                map textures/littlemexico/window3.tga
                rgbGen identity
                depthWrite
                blendFunc Blend  
        }
        {
                map $lightmap 
                blendfunc filter
                rgbGen identity
                tcGen lightmap 
                depthFunc equal
        }
}

textures/littlemexico/manhole2
{
        qer_editorimage textures/littlemexico/manhole2.tga

        surfaceparm alphashadow
        surfaceparm nomarks
        surfaceparm trans
        cull disable
        nopicmip

        {
                map textures/littlemexico/manhole2.tga
                rgbGen identity
                depthWrite
                blendFunc Blend  
        }
        {
                map $lightmap 
                blendfunc filter
                rgbGen identity
                tcGen lightmap 
                depthFunc equal
        }
}
textures/littlemexico/deco1
{
        qer_editorimage textures/littlemexico/deco1.tga

        surfaceparm alphashadow
        surfaceparm nomarks
        surfaceparm trans
        cull disable
        nopicmip

        {
                map textures/littlemexico/deco1.tga
                rgbGen identity
                depthWrite
                blendFunc Blend  
        }
        {
                map $lightmap 
                blendfunc filter
                rgbGen identity
                tcGen lightmap 
                depthFunc equal
        }
}
textures/littlemexico/jumpfx_light
{
        q3map_surfacelight 50
	surfaceparm nomarks
	surfaceparm trans
	cull disable
	{
		map textures/littlemexico/jumpfx_light.tga
		blendfunc add
		rgbGen wave triangle 0.2 0.5 0 0.2
	}
}
textures/littlemexico/fish
{
        qer_editorimage textures/littlemexico/fish.tga

        surfaceparm alphashadow
        surfaceparm nomarks
        surfaceparm trans
        cull disable
        nopicmip

        {
                map textures/littlemexico/fish.tga
                rgbGen identity
                depthWrite
                blendFunc Blend  
        }
        {
                map $lightmap 
                blendfunc filter
                rgbGen identity
                tcGen lightmap 
                depthFunc equal
        }
}
