
textures/metal/mercury
{
	qer_editorimage textures/metal/mercury.png
	surfaceparm nomarks
	surfaceparm lava
	surfaceparm nolightmap
	surfaceparm trans
	deformVertexes wave 150 sin 0 .4 0 .4
	tessSize 24
	q3map_globaltexture
	{
		map textures/metal/mercury.png
		tcgen environment
		//tcMod scroll 0.01 0.01
		tcmod scale 2 2
		blendfunc blend
	}
	dp_reflect 1 1 1 1 0.3
}
