
textures/misc/star-field
{
	qer_editorimage textures/misc/star-field.png
	qer_nocarve
	surfaceparm noimpact
	surfaceparm noclip
	surfaceparm nolightmap
	surfaceparm detail
	cull disable
	{
		map textures/misc/star-field.png
		tcMod turb 0 0.2 0 0.04
	}
}
