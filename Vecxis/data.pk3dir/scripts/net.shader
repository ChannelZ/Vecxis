 
textures/net/net-white
{
	qer_trans 0.40
	surfacepram aphashadow
	surfacepram trans
	surfacepram nodamage
	surfacepram nomarks
	surfacepram nonsolid
	surfacepram playerclip
	
	deformVertexes wave 32 sin 0 1 0 1
	tessSize 32
	cull disable
	
	
	{
		map textures/net/net-white.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}

textures/net/net-black
{
	qer_trans 0.40
	surfacepram aphashadow
	surfacepram trans
	surfacepram nodamage
	surfacepram nomarks
	surfacepram nonsolid
	surfacepram playerclip
	
	deformVertexes wave 32 sin 0 1 0 1
	tessSize 32
	cull disable
	
	
	{
		map textures/net/net-black.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}

textures/net/net-blue
{
	qer_trans 0.40
	surfacepram aphashadow
	surfacepram trans
	surfacepram nodamage
	surfacepram nomarks
	surfacepram nonsolid
	surfacepram playerclip
	
	deformVertexes wave 32 sin 0 1 0 1
	tessSize 32
	cull disable
	
	
	{
		map textures/net/net-blue.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}

textures/net/net-green
{
	qer_trans 0.40
	surfacepram aphashadow
	surfacepram trans
	surfacepram nodamage
	surfacepram nomarks
	surfacepram nonsolid
	surfacepram playerclip
	
	deformVertexes wave 32 sin 0 1 0 1
	tessSize 32
	cull disable
	
	
	{
		map textures/net/net-green.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}

textures/net/net-pink
{
	qer_trans 0.40
	surfacepram aphashadow
	surfacepram trans
	surfacepram nodamage
	surfacepram nomarks
	surfacepram nonsolid
	surfacepram playerclip
	
	deformVertexes wave 32 sin 0 1 0 1
	tessSize 32
	cull disable
	
	
	{
		map textures/net/net-pink.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}

textures/net/net-purple
{
	qer_trans 0.40
	surfacepram aphashadow
	surfacepram trans
	surfacepram nodamage
	surfacepram nomarks
	surfacepram nonsolid
	surfacepram playerclip
	
	deformVertexes wave 32 sin 0 1 0 1
	tessSize 32
	cull disable
	
	
	{
		map textures/net/net-purple.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}

textures/net/net-red
{
	qer_trans 0.40
	surfacepram aphashadow
	surfacepram trans
	surfacepram nodamage
	surfacepram nomarks
	surfacepram nonsolid
	surfacepram playerclip
	
	deformVertexes wave 32 sin 0 1 0 1
	tessSize 32
	cull disable
	
	
	{
		map textures/net/net-red.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}
textures/net/net-tie-dye
{
	qer_trans 0.40
	surfacepram aphashadow
	surfacepram trans
	surfacepram nodamage
	surfacepram nomarks
	surfacepram nonsolid
	surfacepram playerclip
	
	deformVertexes wave 32 sin 0 1 0 1
	tessSize 32
	cull disable
	
	
	{
		map textures/net/net-tie-dye.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}

textures/net/net-white
{
	qer_trans 0.40
	surfacepram aphashadow
	surfacepram trans
	surfacepram nodamage
	surfacepram nomarks
	surfacepram nonsolid
	surfacepram playerclip
	
	deformVertexes wave 32 sin 0 1 0 1
	tessSize 32
	cull disable
	
	
	{
		map textures/net/net-white.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}

textures/net/net-yellow
{
	qer_trans 0.40
	surfacepram aphashadow
	surfacepram trans
	surfacepram nodamage
	surfacepram nomarks
	surfacepram nonsolid
	surfacepram playerclip
	
	deformVertexes wave 32 sin 0 1 0 1
	tessSize 32
	cull disable
	
	
	{
		map textures/net/net-yellow.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}