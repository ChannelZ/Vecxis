// shaders for the Project::OSiRiON skybox collection

textures/skies_osirion/sky01
{
      qer_editorimage env/osirion/sky01.png
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky01 - -
}

textures/skies_osirion/sky02
{
      qer_editorimage env/osirion/sky02
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky02 - -
}

textures/skies_osirion/sky03
{
      qer_editorimage env/osirion/sky03
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky03 - -
}

textures/skies_osirion/sky04
{
      qer_editorimage env/osirion/sky04
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky04 - -
}

textures/skies_osirion/sky05
{
      qer_editorimage env/osirion/sky05
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky05 - -
}

textures/skies_osirion/sky06
{
      qer_editorimage env/osirion/sky06
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky06 - -
}

textures/skies_osirion/sky07
{
      qer_editorimage env/osirion/sky07
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky07 - -
}

textures/skies_osirion/sky08
{
      qer_editorimage env/osirion/sky08
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky08 - -
}

textures/skies_osirion/sky09
{
      qer_editorimage env/osirion/sky09
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky09 - -
}

textures/skies_osirion/sky10
{
      qer_editorimage env/osirion/sky10
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky10 - -
}

textures/skies_osirion/sky11
{
      qer_editorimage env/osirion/sky11
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky11 - -
}

textures/skies_osirion/sky12
{
      qer_editorimage env/osirion/sky12
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky12 - -
}

textures/skies_osirion/sky13
{
      qer_editorimage env/osirion/sky13
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky13 - -
}

textures/skies_osirion/sky14
{
      qer_editorimage env/osirion/sky14
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky14 - -
}

textures/skies_osirion/sky15
{
      qer_editorimage env/osirion/sky15
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky15 - -
}

textures/skies_osirion/sky16
{
      qer_editorimage env/osirion/sky16
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky16 - -
}

textures/skies_osirion/sky17
{
      qer_editorimage env/osirion/sky17
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky17 - -
}

textures/skies_osirion/sky18
{
      qer_editorimage env/osirion/sky18
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky18 - -
}

textures/skies_osirion/sky19
{
      qer_editorimage env/osirion/sky19
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky19 - -
}

textures/skies_osirion/sky20
{
      qer_editorimage env/osirion/sky20
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky20 - -
}

textures/skies_osirion/sky21
{
      qer_editorimage env/osirion/sky21
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky21 - -
}

textures/skies_osirion/sky22
{
      qer_editorimage env/osirion/sky22
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky22 - -
}

textures/skies_osirion/sky23
{
      qer_editorimage env/osirion/sky23
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky23 - -
}

textures/skies_osirion/sky24
{
      qer_editorimage env/osirion/sky24
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky24 - -
}

textures/skies_osirion/sky25
{
      qer_editorimage env/osirion/sky25
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky25 - -
}

textures/skies_osirion/sky26
{
      qer_editorimage env/osirion/sky26
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky26 - -
}

textures/skies_osirion/sky27
{
      qer_editorimage env/osirion/sky27
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky27 - -
}

textures/skies_osirion/sky28
{
      qer_editorimage env/osirion/sky28
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky28 - -
}

textures/skies_osirion/sky29
{
      qer_editorimage env/osirion/sky29
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky29 - -
}

textures/skies_osirion/sky30
{
      qer_editorimage env/osirion/sky30
      surfaceparm noimpact
      surfaceparm nolightmap
      surfaceparm sky
      
      skyparms env/osirion/sky30 - -
}