
textures/skies/clouds1
{
	qer_editorimage env/clouds1/clouds1_rt.png

	surfaceparm nodlight
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm sky
	
	q3map_surfacelight 175
	q3map_sun 1.00 1.00 1.00 85 60 25

	skyparms env/clouds1/clouds1 - -
}

textures/skies_westbeam/westbeam0
{
	qer_editorimage env/space/westbeam0_ft.png

	surfaceparm nodlight
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm sky

	q3map_surfacelight 65

	skyparms env/space/westbeam0 - -
}

textures/skies_westbeam/westbeam1
{
	qer_editorimage env/space/westbeam1_ft.png
	
	surfaceparm nodlight
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm sky

	q3map_surfacelight 65

	skyparms env/space/westbeam1 - -
}

textures/skies_westbeam/westbeam2
{
	qer_editorimage env/space/westbeam2_ft.png
	
	surfaceparm nodlight
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm sky

	q3map_surfacelight 65

	skyparms env/space/westbeam2 - -
}

//////////////////////////////////////////////////////
// The folowing shaders where added from other .shader files
//////////////////////////////////////////////////////


// Direction & elevation checked and adjusted - Speaker
textures/skies/ely_nevada
{
	qer_editorimage env/ely_nevada/nevada_ft.tga
	surfaceparm noimpact
	surfaceparm nolightmap
	q3map_globaltexture
	q3map_lightsubdivide 256
	surfaceparm sky
	q3map_surfacelight 90
	q3map_sun 1 1 1 200 170 40
	skyparms env/ely_nevada/nevada - -
}

textures/skies/asteroids
{
	qer_editorimage env/asteroids/asteroids.tga
	surfaceparm sky
	surfaceparm nolightmap
	surfaceparm nodlight
	surfaceparm noimpact
	surfaceparm nomarks

	skyparms env/asteroids/asteroids - - //farbox cloudheight nearbox
	q3map_sunExt 1 0.75 0.5 320 27 42 0 16 //rgb intensity degrees elevation deviance samples
}

textures/skies/nebulae
{
	qer_editorimage env/nebulae/nebulae.tga
	surfaceparm sky
	surfaceparm nolightmap
	surfaceparm nodlight
	surfaceparm noimpact
	surfaceparm nomarks

	skyparms env/nebulae/nebulae - - //farbox cloudheight nearbox
	q3map_skyLight 50 4 //amount iterations
	q3map_sunExt 1 0.9 0.8 300 66 36 3 16 //rgb intensity degrees elevation deviance samples
}

textures/skies/planets
{
	qer_editorimage env/planets/planets.tga
	surfaceparm sky
	surfaceparm nolightmap
	surfaceparm nodlight
	surfaceparm noimpact
	surfaceparm nomarks

	skyparms env/planets/planets - - //farbox cloudheight nearbox
	q3map_sunExt 1 0.9 0.8 320 124 42 0 16 //rgb intensity degrees elevation deviance samples
}

//By Cuinn (cuinnton) Herrick - GPL - A polluted earth covered in a thick hase.
textures/skies/polluted_earth
{
	qer_editorimage env/polluted_earth/polluted_earth.jpg
	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks
	q3map_globaltexture
	q3map_surfacelight 120
	surfaceparm sky

	nopicmip
	nomipmaps

	q3map_sunExt 1 0.9 0.9 250 320 30
	q3map_skylight 100 4
	skyparms env/polluted_earth/polluted_earth - -
}
