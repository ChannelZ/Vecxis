 
textures/vecxis/vecxis-dark-144x144
{
	qer_trans 0.3
	
	surfaceparm alphashadow
	surfaceparm trans
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm nodlight
	surfaceparm nolightmap
	
	cull none

	
	{
	map textures/vecxis/vecxis-dark-144x144.png
	blendFunc GL_ONE GL_ONE_MINUS_SRC_ALPHA
	}
}

textures/vecxis/vecxis-dark-1024x1024
{
	qer_trans 0.3
	
	surfaceparm alphashadow
	surfaceparm trans
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm nodlight
	surfaceparm nolightmap
	
	cull none

	
	{
	map textures/vecxis/vecxis-dark-1024x1024.png
	blendFunc GL_ONE GL_ONE_MINUS_SRC_ALPHA
	}
}
 
textures/vecxis/vecxis-144x144
{
	qer_trans 0.3
	
	surfaceparm alphashadow
	surfaceparm trans
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm nodlight
	surfaceparm nolightmap
	
	cull none

	
	{
	map textures/vecxis/vecxis-144x144.png
	blendFunc GL_ONE GL_ONE_MINUS_SRC_ALPHA
	}
}

textures/vecxis/vecxis-1024x1024
{
	qer_trans 0.3
	
	surfaceparm alphashadow
	surfaceparm trans
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm nodlight
	surfaceparm nolightmap
	
	cull none

	
	{
	map textures/vecxis/vecxis-1024x1024.png
	blendFunc GL_ONE GL_ONE_MINUS_SRC_ALPHA
	}
}