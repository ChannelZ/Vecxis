#!/bin/sh

balance_cfgs="balance_25.cfg balance_std.cfg balance_vc.cfg"

countd=`awk '/^seta? g_/ { print $2; }' default_std.cfg | sort -u | tr -d '\r' | md5sum | cut -c 1-32`
countw=`awk '/^seta? g_/ { print $2; }' balance_std.cfg       | sort -u | tr -d '\r' | md5sum | cut -c 1-32`
for b in $balance_cfgs; do
	countb=`awk '/^seta? g_/ { print $2; }' "$b"  | sort -u | tr -d '\r' | md5sum | cut -c 1-32`
	if [ "$countw" != "$countb" ]; then
		echo "Mismatch between balance.cfg and $b. Aborting."
		exit 1
	fi
done

sed -i -e "s/^set cvar_check_default .*/set cvar_check_default $countd/" default_std.cfg
sed -i -e "s/^set cvar_check_balance .*/set cvar_check_balance $countw/" balance_std.cfg
for b in $balance_cfgs; do
	sed -i -e "s/^set cvar_check_balance .*/set cvar_check_balance $countw/" "$b"
done

sed -e "
	s/^string CVAR_CHECK_DEFAULT = .*/string CVAR_CHECK_DEFAULT = \"$countd\";/;
	s/^string CVAR_CHECK_BALANCE = .*/string CVAR_CHECK_BALANCE = \"$countw\";/;
" ../Vecxis_gamecode/qcsrc/server/constants.qh > ../Vecxis_gamecode/qcsrc/server/constants.qh.new

if ! diff ../Vecxis_gamecode/qcsrc/server/constants.qh ../Vecxis_gamecode/qcsrc/server/constants.qh.new; then
	mv ../Vecxis_gamecode/qcsrc/server/constants.qh.new ../Vecxis_gamecode/qcsrc/server/constants.qh
	if [ -z "$DO_NOT_RUN_MAKE" ]; then
		echo "Done."
	else
		echo "New checksums: $countd, $countw; please recompile!"
	fi
else
	rm -f ../Vecxis_gamecode/qcsrc/server/constants.qh.new
fi
