RMDIR?= rmdir -p
MAPC?= q3map2

MAPNAME?= bloodprison
MAPCOPTS?= -game vecxis -fs_basepath ../../ -fs_game data
BSPOPTS?= -meta -samplesize 8 -mv 1000000 -mi 6000000
SCALEOPTS?= -scale 1.2
VISOPTS?= -vis
LIGHTSCALE?= 0.4
SAMPLES?= 4
LIGHTOPTS?= -light -scale $(LIGHTSCALE) -deluxe -patchshadows -samples $(SAMPLES) -lightmapsize 512  $(BOUNCEOPTS) -bouncegrid
MINIOPTS?= -minimap

all: bsp scale vis light minimap

bsp:
	$(MAPC) $(MAPCOPTS) $(BSPOPTS) $(MAPNAME).map
	
scale: bsp
	$(MAPC) $(MAPCOPTS) $(SCALEOPTS) $(MAPNAME).bsp
	mv $(MAPNAME)_s.bsp $(MAPNAME).bsp

vis: bsp scale
	$(MAPC) $(MAPCOPTS) $(VISOPTS) $(MAPNAME).bsp

light: bsp vis scale
	$(MAPC) $(MAPCOPTS) $(LIGHTOPTS) $(MAPNAME).bsp

minimap: bsp vis light scale
	$(MAPC) $(MAPCOPTS) $(MINIOPTS) $(MAPNAME).bsp

clean:
	$(RM) $(MAPNAME).bsp $(MAPNAME).prt $(MAPNAME).srf $(MAPNAME)/*.tga
	