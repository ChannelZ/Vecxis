RMDIR?= rmdir -p
MAPC?= q3map2

MAPNAME?= cyberparcour01
MAPCOPTS?= -game vecxis -fs_basepath ../../ -fs_game data
BSPOPTS?= -meta -samplesize 8 -mv 1000000 -mi 6000000
VISOPTS?= -vis
LIGHTSCALE?= 0.333333
SAMPLES?= 4
LIGHTOPTS?= -light -scale $(LIGHTSCALE) -deluxe -patchshadows -samples $(SAMPLES) -lightmapsize 512  $(BOUNCEOPTS) -bouncegrid
MINIOPTS?= -minimap

all: bsp light minimap

bsp:
	$(MAPC) $(MAPCOPTS) $(BSPOPTS) $(MAPNAME).map

light: bsp
	$(MAPC) $(MAPCOPTS) $(LIGHTOPTS) $(MAPNAME).bsp

minimap: bsp light
	$(MAPC) $(MAPCOPTS) $(MINIOPTS) $(MAPNAME).bsp

clean:
	$(RM) $(MAPNAME).bsp $(MAPNAME).prt $(MAPNAME).srf $(MAPNAME)/*.tga
	