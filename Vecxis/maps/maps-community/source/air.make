RMDIR?= rmdir -p -p
MAPC?= q3map2

MAPNAME?= air
MAPCOPTS?= -game vecxis -fs_basepath ../../ -fs_game data
BSPOPTS?= -meta -samplesize 8 -mv 1000000 -mi 6000000
LIGHTSCALE?= 0.333333
SAMPLES?= 4
BOUNCEOPTS?= -bounce 16
LIGHTOPTS?= -light -scale $(LIGHTSCALE) -deluxe -patchshadows -samples $(SAMPLES) -lightmapsize 512  $(BOUNCEOPTS) -bouncegrid
MINIOPTS?= -minimap

all: bsp light minimap

bsp:
	$(MAPC) $(MAPCOPTS) $(BSPOPTS) $(MAPNAME).map

# This ENTIRE map is open, running the vis stage is the incarnation of stupid.

light: bsp
	$(MAPC) $(MAPCOPTS) $(LIGHTOPTS) $(MAPNAME).bsp

minimap: bsp light
	$(MAPC) $(MAPCOPTS) $(MINIOPTS) $(MAPNAME).bsp

clean:
	$(RM) $(MAPNAME).bsp $(MAPNAME).prt $(MAPNAME).srf $(MAPNAME)/*.tga
	