"//campaign:Vecxis 1c Campaign"
"//game","mapname","bots","skill","frag","mutator-sets","description","long description"
"dm","starship","5","7","25",,"DM: Starship","This is the qualifying round for the 2174 Vecxis tournament.\nOnly those able to overcome this slight challenge may proceed."\
"lms","yakmilk","11","8","10",,"LMS: Yakmilk","Congratulations for your acceptance into the 2174 tournament!\nNow the real fights begin.\nPrepare to enter the YakMilk arena!"
"tdm","warfare","5","7","25",,"TDM: Warfare","It's not all about you anymore! In the next round you'll be battling it out in a two team brawl."\
