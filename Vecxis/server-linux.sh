#!/usr/bin/env bash

case "$(uname -m)" in
  x86_64)	executable="vecxis-linux-x86_64-dedicated" ;;
  armv6l)	executable="vecxis-linux-armv6l-dedicated" ;;
  *)		executable="vecxis-linux-i686-dedicated" ;;
esac

cd "`dirname "${0}"`"

if ! [ -x "$executable" ]; then
	if [ -x "$executable" ] && { [ -f ~/.vecxis/data/server.cfg ] || [ -f data/server.cfg ]; }; then
		cd ..
	else
		echo "This script is not properly set up yet."
		echo "Please refer to the instructions in readme.txt, or check the Vecxis Wiki/Forums."
		echo "Wiki: www.vecxis.com/wiki"
		echo "Forums: forums.vecxis.com"
		exit 1
	fi
fi

# WARNING: Strange code!
# If the user specifies +serverconfig as the FIRST argument, we UN-SET $serverconfig then pass all arguments to the executable.
$serverconfig="+serverconfig server.cfg"
if [ $1 == "+serverconfig" ]; then
	$serverconfig=""
fi

exec ./${executable} $serverconfig "${@}"
