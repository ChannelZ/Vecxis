These files are licensed: 
	http://creativecommons.org/publicdomain/zero/1.0/
They come from:
	http://opengameart.org/content/50-free-textures-4-normalmaps
	http://opengameart.org/content/50-free-textures-5-with-normalmaps
		The first 25 sources from pack 5 (images 201-250) are said to have been taken from:
			http://opengameart.org/content/avoid-the-germs-hunt-the-gems-3-misc-unsorted-texture-sources
