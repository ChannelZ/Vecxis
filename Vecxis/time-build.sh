#!/bin/sh

#Example to run with 8 processes and low priority: nice nice ./time-build.sh 8


#How many jobs should make run?
#NOTE: Many parts of the Vecxis(TM) build system try to auto-detect a computer's parallel capabilities. This does little to limit the number of running processes. It's really only good for limiting the amount of maps built in parallel. Currently about 2GB of memory is used per map during compile, so figure Memory Used = 2GB * $JOBS
if [ $1 ]
then
	JOBS="-j$1"
else
	JOBS="-j`nproc`"
fi

#Remove the old log file.
rm build-times.log
rm time-build-err.log
#Log the start time.
echo "Start: "`date` >> build-times.log
#Build Vecxis, sending stderr to err.log
time make $JOBS all BUILD_ENGINE_LINUX_64=true BUILD_ENGINE_LINUX_32=true BUILD_ENGINE_WINDOWS_32=true CONVERTOPTS=" " PACKAGE_CGF_ENABLED=true PACKAGE_CGF_ENABLED_MEGA=true 2> time-build-err.log
#Log the end time.
echo "Stop: "`date` >> build-times.log

# PACKAGE_CGF_ENABLED=true PACKAGE_CGF_ENABLED_MEGA=true
